package lack.before;

import lack.before.model.Group;
import lack.before.service.GroupService;
import lack.before.service.UserService;

public class Consumer1 {
	private static String CURRENT_USER_ID = "4";

	public boolean isCurrentUserAdmin() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		GroupService service = new GroupService(userService, dataSource);

		Group group = service.getGroupByUserName(CURRENT_USER_ID);

		return group.isAdmin();
	}
}
