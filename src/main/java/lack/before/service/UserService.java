package lack.before.service;

import lack.before.model.User;

public class UserService {
	private final DataSource dataSource;

	public UserService(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public User getUserByName(String name) {
		User user = this.dataSource.getData("user", name);

		return user;
	}
}
