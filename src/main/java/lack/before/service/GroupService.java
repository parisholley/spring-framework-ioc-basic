package lack.before.service;

import lack.before.model.Group;
import lack.before.model.User;

public class GroupService {
	private final UserService userService;
	private final DataSource dataSource;

	public GroupService(UserService userService, DataSource dataSource) {
		this.userService = userService;
		this.dataSource = dataSource;
	}

	public Group getGroupByUserName(String name) {
		User user = this.userService.getUserByName(name);

		if( user == null){
			return null;
		}
		
		return this.getGroupById(user.getGroupId());
	}

	protected Group getGroupById(String id) {
		Group group = this.dataSource.getData("group", id);

		return group;
	}
}
