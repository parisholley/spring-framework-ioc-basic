package lack.before.model;

public class Group {
	private boolean admin;
	private String id;

	public Group() {

	}

	public Group(String id, boolean admin) {
		this.setId(id);
		this.setAdmin(admin);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
}
