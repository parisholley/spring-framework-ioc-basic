package lack.after.model;

public class User {
	private String id;
	private String name;
	private String groupId;

	public User() {

	}

	public User(String name, String id, String groupId) {
		this.setId(id);
		this.setName(name);
		this.setGroupId(groupId);
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
