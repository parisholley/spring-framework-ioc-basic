package lack.after;

import lack.after.dao.GroupDao;
import lack.after.dao.UserDao;
import lack.after.model.Group;
import lack.after.service.GroupService;
import lack.after.service.UserService;

public class Consumer1 {
	private static String CURRENT_USER_ID = "4";

	public boolean isCurrentUserAdmin() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		UserService userService = new UserService();
		userService.setDao(userDao);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		GroupService service = new GroupService();
		service.setDao(dao);
		service.setUserService(userService);

		Group group = service.getGroupByUserName(CURRENT_USER_ID);

		return group.isAdmin();
	}
}
