package lack.after.service;

import lack.after.dao.UserDao;
import lack.after.model.User;

public class UserService {
	private UserDao dao;

	public UserDao getDao() {
		return dao;
	}

	public void setDao(UserDao dao) {
		this.dao = dao;
	}

	public User getUserByName(String name) {
		User user = this.dao.getUserByName(name);

		return user;
	}
}
