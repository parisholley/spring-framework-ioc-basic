package springimpl.before;

import springimpl.before.service.DataSource;

public class DataSourceFactory {
	public static String DATA_SOURCE_PROPERTY = "database";
	public static String DATA_SOURCE_PROPERTY_MEMORY = "memory";
	public static String DATA_SOURCE_PROPERTY_FILE = "file";

	protected static DataSourceImpl DATA_SOURCE_MEMORY;
	protected static DataSourceFile DATA_SOURCE_FILE;

	static {
		DATA_SOURCE_MEMORY = new DataSourceImpl();
		DATA_SOURCE_MEMORY.initializeData();

		DATA_SOURCE_FILE = new DataSourceFile();
		DATA_SOURCE_FILE.initializeData();
	}

	public static DataSource getDataSource() {
		if (System.getProperty(DATA_SOURCE_PROPERTY, DATA_SOURCE_PROPERTY_MEMORY).equals(DATA_SOURCE_PROPERTY_MEMORY)) {
			return DATA_SOURCE_MEMORY;
		}
		
		if (DATA_SOURCE_PROPERTY_FILE.equals(System.getProperty(DATA_SOURCE_PROPERTY))) {
			return DATA_SOURCE_FILE;
		}

		return null;
	}
}
