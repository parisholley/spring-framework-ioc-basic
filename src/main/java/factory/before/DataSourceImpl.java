package factory.before;

import java.util.HashMap;
import java.util.Map;

import factory.before.model.Group;
import factory.before.model.User;
import factory.before.service.DataSource;

public class DataSourceImpl implements DataSource {
	protected Map<String, Map<String, Object>> dataSets = new HashMap<String, Map<String, Object>>();

	public void initializeData() {
		Map<String, Object> users = new HashMap<String, Object>();
		users.put("test", new User("test", "1", "2"));
		users.put("test2", new User("test2", "4", "3"));
		dataSets.put("user", users);

		Map<String, Object> groups = new HashMap<String, Object>();
		groups.put("2", new Group("2", true));
		groups.put("3", new Group("3", false));
		dataSets.put("group", groups);
	}

	@SuppressWarnings("unchecked")
	public <T> T getData(String dataSet, String id) {
		return (T) dataSets.get(dataSet).get(id);
	}
}
