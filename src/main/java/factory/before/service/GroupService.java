package factory.before.service;

import factory.before.dao.GroupDao;
import factory.before.model.Group;
import factory.before.model.User;

public class GroupService {
	private UserService userService;
	private GroupDao dao;

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GroupDao getDao() {
		return dao;
	}

	public void setDao(GroupDao dao) {
		this.dao = dao;
	}

	public Group getGroupByUserName(String name) {
		User user = this.getUserService().getUserByName(name);

		if (user == null) {
			return null;
		}

		return this.getDao().getGroupById(user.getGroupId());
	}
}
