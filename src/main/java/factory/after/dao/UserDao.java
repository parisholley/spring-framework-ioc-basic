package factory.after.dao;

import factory.after.model.User;
import factory.after.service.DataSource;

public class UserDao {
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public User getUserByName(String name) {
		User user = this.dataSource.getData("user", name);

		return user;
	}
}
