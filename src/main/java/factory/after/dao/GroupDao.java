package factory.after.dao;

import factory.after.model.Group;
import factory.after.service.DataSource;

public class GroupDao {
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Group getGroupById(String id) {
		Group group = this.getDataSource().getData("group", id);

		return group;
	}
}
