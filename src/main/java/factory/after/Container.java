package factory.after;

import factory.after.dao.GroupDao;
import factory.after.dao.UserDao;
import factory.after.service.GroupService;
import factory.after.service.UserService;

public class Container {
	private static UserService USER_SERVICE;
	private static UserDao USER_DAO;
	private static GroupDao GROUP_DAO;
	private static GroupService GROUP_SERVICE;

	static {
		USER_DAO = new UserDao();
		USER_DAO.setDataSource(DataSourceFactory.getDataSource());

		USER_SERVICE = new UserService();
		USER_SERVICE.setDao(USER_DAO);

		GROUP_DAO = new GroupDao();
		GROUP_DAO.setDataSource(DataSourceFactory.getDataSource());

		GROUP_SERVICE = new GroupService();
		GROUP_SERVICE.setUserService(USER_SERVICE);
		GROUP_SERVICE.setDao(GROUP_DAO);
	}

	public static UserService getUserService() {
		return USER_SERVICE;
	}

	public static GroupService getGroupService() {
		return GROUP_SERVICE;
	}

	public static GroupDao getGroupDao() {
		return GROUP_DAO;
	}

	public static UserDao getUserDao() {
		return USER_DAO;
	}
}
