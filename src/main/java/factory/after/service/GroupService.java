package factory.after.service;

import factory.after.dao.GroupDao;
import factory.after.model.Group;
import factory.after.model.User;

public class GroupService {
	private UserService userService;
	private GroupDao dao;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GroupDao getDao() {
		return dao;
	}

	public void setDao(GroupDao dao) {
		this.dao = dao;
	}

	public Group getGroupByUserName(String name) {
		User user = this.getUserService().getUserByName(name);

		if (user == null) {
			return null;
		}

		return this.getDao().getGroupById(user.getGroupId());
	}
}
