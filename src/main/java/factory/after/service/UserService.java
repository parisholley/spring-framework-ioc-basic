package factory.after.service;

import factory.after.dao.UserDao;
import factory.after.model.User;

public class UserService {
	private UserDao dao;
	
	public UserDao getDao() {
		return dao;
	}

	public void setDao(UserDao dao) {
		this.dao = dao;
	}

	public User getUserByName(String name) {
		User user = this.dao.getUserByName(name);

		return user;
	}
}
