package factory.after.service;

public interface DataSource {
	public <T> T getData(String dataSet, String id);
}
