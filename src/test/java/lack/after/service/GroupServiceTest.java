package lack.after.service;

import lack.after.dao.GroupDao;
import lack.after.model.Group;
import lack.after.model.User;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class GroupServiceTest {
	@Test
	public void testGetGroupByUserNameNotFound() {
		UserService uservice = Mockito.mock(UserService.class);
		Mockito.when(uservice.getUserByName("test3")).thenReturn(null);

		GroupService service = new GroupService();
		service.setUserService(uservice);

		Group group = service.getGroupByUserName("test3");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByUserNameAdmin() {
		UserService uservice = Mockito.mock(UserService.class);
		Mockito.when(uservice.getUserByName("test")).thenReturn(new User("test","1","2"));

		GroupDao dao = Mockito.mock(GroupDao.class);
		Mockito.when(dao.getGroupById("2")).thenReturn(new Group("2", true));
		
		GroupService service = new GroupService();
		service.setUserService(uservice);
		service.setDao(dao);
		
		Group group = service.getGroupByUserName("test");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
		Assert.assertTrue(group.isAdmin());
	}

	@Test
	public void testGetGroupByUserNameNotAdmin() {
		UserService uservice = Mockito.mock(UserService.class);
		Mockito.when(uservice.getUserByName("test2")).thenReturn(new User("test2","1","3"));

		GroupDao dao = Mockito.mock(GroupDao.class);
		Mockito.when(dao.getGroupById("3")).thenReturn(new Group("3", false));
		
		GroupService service = new GroupService();
		service.setUserService(uservice);
		service.setDao(dao);
		
		Group group = service.getGroupByUserName("test2");

		Assert.assertNotNull(group);
		Assert.assertEquals("3", group.getId());
		Assert.assertFalse(group.isAdmin());
	}
}
