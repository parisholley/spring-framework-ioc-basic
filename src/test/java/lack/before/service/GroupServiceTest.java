package lack.before.service;

import lack.before.DataSourceImpl;
import lack.before.model.Group;

import org.junit.Assert;
import org.junit.Test;

public class GroupServiceTest {
	@Test
	public void testGetGroupByIdNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		GroupService service = new GroupService(userService, dataSource);

		Group group = service.getGroupById("9999");

		Assert.assertNull(group);
	}
	
	@Test
	public void testGetGroupByIdFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		GroupService service = new GroupService(userService, dataSource);

		Group group = service.getGroupById("2");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
	}
	
	@Test
	public void testGetGroupByUserNameNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		GroupService service = new GroupService(userService, dataSource);

		Group group = service.getGroupByUserName("test3");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByUserNameAdmin() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		GroupService service = new GroupService(userService, dataSource);

		Group group = service.getGroupByUserName("test");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
		Assert.assertTrue(group.isAdmin());
	}
	
	@Test
	public void testGetGroupByUserNameNotAdmin() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		GroupService service = new GroupService(userService, dataSource);

		Group group = service.getGroupByUserName("test2");

		Assert.assertNotNull(group);
		Assert.assertEquals("3", group.getId());
		Assert.assertFalse(group.isAdmin());
	}
}
