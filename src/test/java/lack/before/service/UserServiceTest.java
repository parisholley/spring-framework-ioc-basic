package lack.before.service;

import lack.before.DataSourceImpl;
import lack.before.model.User;

import org.junit.Assert;
import org.junit.Test;

public class UserServiceTest {
	@Test
	public void testGetUserByNameNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		User user = userService.getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		UserService userService = new UserService(dataSource);

		User user = userService.getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
