package springimpl.after.dao;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;
import springimpl.after.model.User;

public class UserDaoIntegrationTest extends AbstractIntegrationTest {	
	@Test
	public void testGetUserByNameNotFound() {
		User user = this.getContext().getBean(UserDao.class).getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		User user = this.getContext().getBean(UserDao.class).getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
