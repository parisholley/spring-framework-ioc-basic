package springimpl.after.dao;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;
import springimpl.after.model.Group;

public class GroupDaoIntegrationTest extends AbstractIntegrationTest {
	@Test
	public void testGetGroupByIdNotFound() {
		Group group = this.getContext().getBean(GroupDao.class).getGroupById("9999");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByIdFound() {
		Group group = this.getContext().getBean(GroupDao.class).getGroupById("2");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
	}
}
