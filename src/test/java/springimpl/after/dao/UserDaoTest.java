package springimpl.after.dao;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import springimpl.after.model.User;
import springimpl.after.service.DataSource;

public class UserDaoTest {
	@Test
	public void testGetUserByNameNotFound() {
		DataSource ds = Mockito.mock(DataSource.class);
		Mockito.when(ds.getData("user", "foobar")).thenReturn(null);

		UserDao dao = new UserDao();
		dao.setDataSource(ds);

		User user = dao.getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		DataSource ds = Mockito.mock(DataSource.class);
		Mockito.when(ds.getData("user", "test")).thenReturn(new User("test", null, null));

		UserDao dao = new UserDao();
		dao.setDataSource(ds);

		User user = dao.getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
