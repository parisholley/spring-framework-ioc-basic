package springimpl.after;

import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AbstractIntegrationTest {
	private static ApplicationContext CONTEXT;
	
	@BeforeClass
	public static void setupSpring() {
		if( CONTEXT == null){
			CONTEXT = new ClassPathXmlApplicationContext(new String[] { "/springimpl/after/applicationContext.xml" });
		}
	}
	
	protected ApplicationContext getContext(){
		return CONTEXT;
	}
}
