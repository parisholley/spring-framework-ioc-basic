package springimpl.after.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import springimpl.after.dao.UserDao;
import springimpl.after.model.User;

public class UserServiceTest {
	@Test
	public void testGetUserByNameNotFound() {
		UserDao dao = Mockito.mock(UserDao.class);
		Mockito.when(dao.getUserByName("foobar")).thenReturn(null);

		UserService service = new UserService();
		service.setDao(dao);

		User user = service.getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		UserDao dao = Mockito.mock(UserDao.class);
		Mockito.when(dao.getUserByName("test")).thenReturn(new User("test", "1", "2"));

		UserService service = new UserService();
		service.setDao(dao);

		User user = service.getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
