package springimpl.after.service;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;
import springimpl.after.model.User;

public class UserServiceIntegrationTest extends AbstractIntegrationTest {
	@Test
	public void testGetUserByNameNotFound() {
		User user = this.getContext().getBean(UserService.class).getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		User user = this.getContext().getBean(UserService.class).getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
