package springimpl.before.dao;

import org.junit.Assert;
import org.junit.Test;

import springimpl.before.AbstractTest;
import springimpl.before.Container;
import springimpl.before.model.Group;

public class GroupDaoTest extends AbstractTest {
	@Test
	public void testGetGroupByIdNotFound() {
		Group group = Container.getGroupDao().getGroupById("9999");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByIdFound() {
		Group group = Container.getGroupDao().getGroupById("2");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
	}
}
