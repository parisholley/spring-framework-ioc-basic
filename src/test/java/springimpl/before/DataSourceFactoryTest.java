package springimpl.before;

import org.junit.Assert;
import org.junit.Test;

import springimpl.before.service.DataSource;

public class DataSourceFactoryTest {
	@Test
	public void testGetDataSourceUndefined() {
		System.clearProperty(DataSourceFactory.DATA_SOURCE_PROPERTY);

		DataSource ds = DataSourceFactory.getDataSource();
		
		Assert.assertNotNull(ds);
		Assert.assertEquals(DataSourceImpl.class, ds.getClass());
	}

	@Test
	public void testGetDataSourceMemory() {
		System.setProperty(DataSourceFactory.DATA_SOURCE_PROPERTY, DataSourceFactory.DATA_SOURCE_PROPERTY_MEMORY);

		DataSource ds = DataSourceFactory.getDataSource();

		Assert.assertNotNull(ds);
		Assert.assertEquals(DataSourceImpl.class, ds.getClass());
	}
	
	@Test
	public void testGetDataSourceFile() {
		System.setProperty(DataSourceFactory.DATA_SOURCE_PROPERTY, DataSourceFactory.DATA_SOURCE_PROPERTY_FILE);

		DataSource ds = DataSourceFactory.getDataSource();

		Assert.assertNotNull(ds);
		Assert.assertEquals(DataSourceFile.class, ds.getClass());
	}
}
