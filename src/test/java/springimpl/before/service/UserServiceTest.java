package springimpl.before.service;

import org.junit.Assert;
import org.junit.Test;

import springimpl.before.AbstractTest;
import springimpl.before.Container;
import springimpl.before.model.User;

public class UserServiceTest extends AbstractTest {
	@Test
	public void testGetUserByNameNotFound() {
		User user = Container.getUserService().getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		User user = Container.getUserService().getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
