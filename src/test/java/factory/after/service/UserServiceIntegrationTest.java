package factory.after.service;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;
import factory.after.Container;
import factory.after.model.User;

public class UserServiceIntegrationTest extends AbstractIntegrationTest {
	@Test
	public void testGetUserByNameNotFound() {
		User user = Container.getUserService().getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		User user = Container.getUserService().getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
