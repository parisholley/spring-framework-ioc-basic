package factory.after.service;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;
import factory.after.Container;
import factory.after.model.Group;

public class GroupServiceIntegrationTest extends AbstractIntegrationTest {
	@Test
	public void testGetGroupByUserNameNotFound() {
		Group group = Container.getGroupService().getGroupByUserName("test3");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByUserNameAdmin() {
		Group group = Container.getGroupService().getGroupByUserName("test");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
		Assert.assertTrue(group.isAdmin());
	}

	@Test
	public void testGetGroupByUserNameNotAdmin() {
		Group group = Container.getGroupService().getGroupByUserName("test2");

		Assert.assertNotNull(group);
		Assert.assertEquals("3", group.getId());
		Assert.assertFalse(group.isAdmin());
	}
}
