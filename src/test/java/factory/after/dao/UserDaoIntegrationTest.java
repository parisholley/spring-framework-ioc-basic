package factory.after.dao;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;
import factory.after.Container;
import factory.after.model.User;

public class UserDaoIntegrationTest extends AbstractIntegrationTest {
	@Test
	public void testGetUserByNameNotFound() {
		User user = Container.getUserDao().getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		User user = Container.getUserDao().getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
