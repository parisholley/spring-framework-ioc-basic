package factory.after.dao;

import org.junit.Assert;
import org.junit.Test;

import springimpl.after.AbstractIntegrationTest;

import factory.after.Container;
import factory.after.model.Group;

public class GroupDaoIntegrationTest extends AbstractIntegrationTest {
	@Test
	public void testGetGroupByIdNotFound() {
		Group group = Container.getGroupDao().getGroupById("9999");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByIdFound() {
		Group group = Container.getGroupDao().getGroupById("2");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
	}
}
