package factory.after.dao;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import factory.after.model.Group;
import factory.after.service.DataSource;

public class GroupDaoTest {
	@Test
	public void testGetGroupByIdNotFound() {
		DataSource ds = Mockito.mock(DataSource.class);
		Mockito.when(ds.getData("group", "9999")).thenReturn(null);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(ds);
				
		Group group = dao.getGroupById("9999");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByIdFound() {
		DataSource ds = Mockito.mock(DataSource.class);
		Mockito.when(ds.getData("group", "2")).thenReturn(new Group("2", false));
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(ds);
		
		Group group = dao.getGroupById("2");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
	}
}
