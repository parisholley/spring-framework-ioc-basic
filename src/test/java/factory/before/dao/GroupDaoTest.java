package factory.before.dao;

import org.junit.Assert;
import org.junit.Test;

import factory.before.DataSourceImpl;
import factory.before.model.Group;

public class GroupDaoTest {
	@Test
	public void testGetGroupByIdNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		Group group = dao.getGroupById("9999");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByIdFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();

		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		Group group = dao.getGroupById("2");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
	}
}
