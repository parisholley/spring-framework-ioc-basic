package factory.before.dao;

import org.junit.Assert;
import org.junit.Test;

import factory.before.DataSourceImpl;
import factory.before.model.User;

public class UserDaoTest {
	@Test
	public void testGetUserByNameNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		User user = userDao.getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		User user = userDao.getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
