package factory.before.service;

import org.junit.Assert;
import org.junit.Test;

import factory.before.DataSourceImpl;
import factory.before.dao.GroupDao;
import factory.before.dao.UserDao;
import factory.before.model.Group;

public class GroupServiceTest {
	@Test
	public void testGetGroupByUserNameNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		UserService userService = new UserService();
		userService.setDao(userDao);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		GroupService service = new GroupService();
		service.setDao(dao);
		service.setUserService(userService);

		Group group = service.getGroupByUserName("test3");

		Assert.assertNull(group);
	}

	@Test
	public void testGetGroupByUserNameAdmin() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		UserService userService = new UserService();
		userService.setDao(userDao);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		GroupService service = new GroupService();
		service.setDao(dao);
		service.setUserService(userService);

		Group group = service.getGroupByUserName("test");

		Assert.assertNotNull(group);
		Assert.assertEquals("2", group.getId());
		Assert.assertTrue(group.isAdmin());
	}
	
	@Test
	public void testGetGroupByUserNameNotAdmin() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		UserService userService = new UserService();
		userService.setDao(userDao);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		GroupService service = new GroupService();
		service.setDao(dao);
		service.setUserService(userService);

		Group group = service.getGroupByUserName("test2");

		Assert.assertNotNull(group);
		Assert.assertEquals("3", group.getId());
		Assert.assertFalse(group.isAdmin());
	}
}
