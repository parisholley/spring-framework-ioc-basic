package factory.before.service;

import org.junit.Assert;
import org.junit.Test;

import factory.before.DataSourceImpl;
import factory.before.dao.GroupDao;
import factory.before.dao.UserDao;
import factory.before.model.User;

public class UserServiceTest {
	@Test
	public void testGetUserByNameNotFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		UserService userService = new UserService();
		userService.setDao(userDao);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		GroupService service = new GroupService();
		service.setDao(dao);
		service.setUserService(userService);

		User user = userService.getUserByName("foobar");

		Assert.assertNull(user);
	}

	@Test
	public void testGetUserByNameFound() {
		DataSourceImpl dataSource = new DataSourceImpl();
		dataSource.initializeData();
		
		UserDao userDao = new UserDao();
		userDao.setDataSource(dataSource);

		UserService userService = new UserService();
		userService.setDao(userDao);
		
		GroupDao dao = new GroupDao();
		dao.setDataSource(dataSource);

		GroupService service = new GroupService();
		service.setDao(dao);
		service.setUserService(userService);

		User user = userService.getUserByName("test");

		Assert.assertNotNull(user);
		Assert.assertEquals("test", user.getName());
	}
}
